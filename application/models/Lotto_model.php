<?php
class Lotto_model extends CI_model{

	public function date_lottory($lottory){
		$this->load->database('default');
		$this->db->select('*');
		$this->db->from('lottory');
		$this->db->where('date_lottory',$lottory);
		$query=$this->db->get();

		if($query->num_rows()>0){
			return $query->result();
		}else{
			return false; 
		}
	}
	
	public function add_lottory($lottory_all){

		$this->load->database('default');
		$this->db->insert('lottory', $lottory_all);

	}

    public function select_date(){

      $this->load->database('default');
      $this->db->select('id,date_text'); 
      if($query=$this->db->get('lottory'))
      { 
          return $query->result();
      }
      else{
        return false;
      }
    }
    
    public function check_lottory($id){

      $this->load->database('default');
      $this->db->select('*'); 
      $this->db->where('id',$id);
      if($query=$this->db->get('lottory'))
      { 
          return $query->result();
      }
      else{
        return false;
      }
    }
    
    public function check($id){

      $this->load->database('default');
      $this->db->select('*'); 
      $this->db->where('id',$id);
      if($query=$this->db->get('lottory'))
      { 
          return $query->result();
      }
      else{
        return false;
      }
    }


}


?>
