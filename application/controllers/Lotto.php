<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lotto extends CI_Controller {

	public function __construct(){

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('path');
		$this->load->model('lotto_model');
		//$this->load->library('session');
		//$this->load->library('nativesession');
		//$this->load->helper('cookie');

	}
	public function index()
	{
	    $data['select_date'] = $this->lotto_model->select_date();
		$this->load->view('index',$data);
	}
	public function check($lottory)
	{
		include 'Simple_html_dom.php';
		$strlen_lot = strlen($lottory);
		if($strlen_lot != 8){
			$lottory = '0'.$lottory; 
		}
		
		$date_lottory = $this->lotto_model->date_lottory($lottory);
		//มีใน db แล้ว
		if($date_lottory){
			$data['check'] = $date_lottory; 
			
		//ยังไม่มีใน db 
		}else{
			$domain = 'https://news.sanook.com/lotto/check/'.$lottory;
			$url_get = file_get_html($domain);
			$i = 0;
			$front = "";
			$back = "";
			$like_rangwan1 = "";
			$rangwan2 = "";
			$rangwan3 = "";
			$rangwan4 = "";
			$rangwan5 = "";
			foreach($url_get->find('.lotto__number') as $el) { 
				if ($i == 0) { 
					$rangwan1 = $el->plaintext;
				}
				if ($i > 0 and $i <= 2) { 
					$front = $front.$el->plaintext.",";
				}
				if ($i > 2 and $i <= 4) { 
					$back = $back.$el->plaintext.",";
				}
				if ($i == 5) { 
					$two_digits = $el->plaintext;
				}
				if ($i > 5 and $i <= 7) { 
					$like_rangwan1 = $like_rangwan1.$el->plaintext.",";
				}
				if ($i > 7 and $i <= 12) { 
					$rangwan2 = $rangwan2.$el->plaintext.",";
				}
				if ($i > 12 and $i <= 22) { 
					$rangwan3 = $rangwan3.$el->plaintext.",";
				}
				if ($i > 22 and $i <= 72) { 
					$rangwan4 = $rangwan4.$el->plaintext.",";
				}
				if ($i > 72 and $i <= 172) { 
					$rangwan5 = $rangwan5.$el->plaintext.",";
				}
				
				$i++;
			}
			foreach($url_get->find('.content__title--sub') as $el) { 
				$date_text = $el->plaintext;
			}
			$lottory_all=array(
				'rangwan1'=>$rangwan1, 
				'front'=>$front,
				'back'=>$back,
				'two_digits'=>$two_digits,
				'like_rangwan1'=>$like_rangwan1,
				'rangwan2'=>$rangwan2,
				'rangwan3'=>$rangwan3,
				'rangwan4'=>$rangwan4,
				'rangwan5'=>$rangwan5,
				'date_lottory'=>$lottory,
				'date_text'=>$date_text
			);
			$this->lotto_model->add_lottory($lottory_all);
			$data['check'] = $this->lotto_model->date_lottory($lottory);
		}
		$d = substr($lottory,0,2);
		$m = substr($lottory,2,2);
		$y = substr($lottory,4,4);
		switch($m){
			case "01":$mm = " มกราคม "; break; 
			case "02":$mm = " กุมภาพันธ์ "; break;
			case "03":$mm = " มีนาคม "; break; 
			case "04":$mm = " เมษายน "; break;
			case "05":$mm = " พฤษภาคม "; break;
			case "06":$mm = " มิถุนายน "; break;
			case "07":$mm = " กรกฎาคม "; break;
			case "08":$mm = " สิงหาคม "; break;
			case "09":$mm = " กันยายน "; break;
			case "10":$mm = " ตุลาคม "; break;
			case "11":$mm = " พฤศจิกายน "; break;
			case "12":$mm = " ธันวาคม "; break;
		}
		$data['lotto_date'] = 'งวดวันที่ '.$d.$mm.$y;
		$data['select_date'] = $this->lotto_model->select_date(); 
		$this->load->view('check',$data); 
	}
	public function check_lottory() 
	{
		$id = $this->input->post('id');
		$number_lottory = $this->input->post('number_lottory');
        $check_lottory = $this->lotto_model->check_lottory($id);
        $rangwan = "";
		foreach($check_lottory as $el) {
			if ($el->rangwan1 == $number_lottory) {
				$rangwan = 'ถูกรางวัลที่ 1';
			}
			if(strstr($el->like_rangwan1,$number_lottory)){
			    $rangwan = 'ถูกรางวัลข้างเคียงรางวัลที่ 1';
			}
			if(strstr($el->rangwan2,$number_lottory)){
			    $rangwan = 'ถูกรางวรางวัที่ 2';
			}
			if(strstr($el->rangwan3,$number_lottory)){
			    $rangwan = 'ถูกรางวรางวัที่ 3';
			}
			if(strstr($el->rangwan4,$number_lottory)){
			    $rangwan = 'ถูกรางวรางวัที่ 4';
			}
			if(strstr($el->rangwan5,$number_lottory)){
			    $rangwan = 'ถูกรางวรางวัที่ 5';
			}
			$front = substr($number_lottory,0,3);
			if (strstr($el->front,$front)) {
			    if($rangwan == ""){
			        $rangwan = 'ถูกรางวัลเลขหน้า 3 ตัว';
			    }
			    else{
			        $rangwan = $rangwan.' และ ถูกรางวัลเลขหน้า 3 ตัว';
			    }
			}
			$back = substr($number_lottory,3,3);
			if (strstr($el->back,$back)) {
			    if($rangwan == ""){
			        $rangwan = 'ถูกรางวัลเลขท้าย 3 ตัว';
			    }
			    else{
			        $rangwan = $rangwan.' และ ถูกรางวัลเลขท้าย 3 ตัว';
			    } 
			}
			$two_digits = substr($number_lottory,4,2);
			if (strstr($el->two_digits,$two_digits)) {
			    if($rangwan == ""){
			        $rangwan = 'ถูกรางวัลเลขท้าย 2 ตัว';
			    }
			    else{
			        $rangwan = $rangwan.' และ ถูกรางวัลเลขท้าย 2 ตัว';
			    } 
			}
				
		}
		if($rangwan == ""){
		    $rangwan = 'ไม่ถูกรางวัล';
		}
		echo $rangwan; 
		 
	}
}

?>
