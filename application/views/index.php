<!DOCTYPE html>
<html lang="en">
	<!-- ##### HTML TOP Area Start ##### -->
	<?php $this->load->view('html_top'); ?>
<style>
.single-blog-post.featured-post .post-data {
    padding-top: 0;
}
table {
  width: 100%;
  text-align:center;
}
table th {
	display:none;
}
tbody td {
  font-size: 30px;
  color: #3f6814;
  width: 25%;
  font-weight: 700;
}
.back tbody td {
  font-size: 20px;
  color: #3f6814;
  width: 25%;
  font-weight: 700;
}
tbody .ragwan {
  font-size: 12px;
  color: #000;
  width: 25%;
  font-weight: 500;
}
strong{
	display:none;
}
caption{
	display:none;
}
.newsletter-widget select {
    width: 100%;
    height: 50px;
    background-color: #44425a;
    font-size: 14px;
    font-style: italic;
    color: #fff;
    margin-bottom: 25px;
    border: none;
    padding: 0 25px;
}
.newsletter-widget input {
    width: 100%;
    height: 50px;
    background-color: #44425a;
    font-size: 14px;
    font-style: italic;
    color: #fff;
    margin-bottom: 25px;
    border: none;
    padding: 0 25px;
}
 table {
    background: #f8f8f8;
}
@media only screen and (max-width: 767px){
    .mobile_none {
        display:none;
    }
    .tbody td {
        font-size: 25px;
    }
}
@media only screen and (min-width: 768px){
    .pc_none {
        display:none;
    }
}
</style>
<body>
    <!-- ##### Header Area Start ##### -->
	<?php $this->load->view('header'); ?>
    <!-- ##### Header Area End ##### -->

    
    <!--<div class="hero-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-8">
                    
                    <div class="breaking-news-area d-flex align-items-center">
                        <div class="news-title">
                            <p>Breaking News</p>
                        </div>
                        <div id="breakingNewsTicker" class="ticker">
                            <ul>
                                <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                                <li><a href="#">Welcome to Colorlib Family.</a></li>
                                <li><a href="#">Nam eu metus sitsit amet, consec!</a></li>
                            </ul>
                        </div>
                    </div>

                    
                    <div class="breaking-news-area d-flex align-items-center mt-15">
                        <div class="news-title title2">
                            <p>International</p>
                        </div>
                        <div id="internationalTicker" class="ticker">
                            <ul>
                                <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                                <li><a href="#">Welcome to Colorlib Family.</a></li>
                                <li><a href="#">Nam eu metus sitsit amet, consec!</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                
                <div class="col-12 col-lg-4">
                    <div class="hero-add">
                        <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/hero-add.gif" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Featured Post Area Start ##### -->
    <div class="featured-post-area" style="margin-top: 20px;">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-8">
                    <div class="row">

                        <!-- Single Featured Post -->
                        <div class="col-12 col-lg-12">
                            <div class="single-blog-post featured-post">
                                <div class="post-data">
                                    <?php
									 $i = 0;
									 $url = "https://www.lottery.co.th/lotto/feed";
									 $rss = simplexml_load_file($url); // XML parser
									 print '<h2>ตรวจสลากกินแบ่งรัฐบาล</h2>';
									 foreach($rss->channel->item as $item) {
										 if ($i < 1) { // parse only 10 items
											$link_check = str_replace('www.lottery.co.th/lotto' , 'lekden.com/lotto/check' , $item->link);
											$link_check = str_replace('-' , '' , $link_check);  
											$link_check = str_replace('62' , '2562' , $link_check);
											$link_check = str_replace('63' , '2563' , $link_check);
											$link_check = str_replace('64' , '2564' , $link_check);
											$link_check = str_replace('65' , '2565' , $link_check);
											$link_check = str_replace('66' , '2566' , $link_check);
											print '<h2><a href="'.$link_check.'" class="post-catagory" style="font-size: 20px; color: #626262;"title="'.$item->title.'">'.$item->title.'</a></h2><br />'.$item->description.'';
										 }
										 $i++;
									 }
									 ?>
									 <div class="table-responsive">
										 <table>
											 <tbody>
												<tr class="mobile_none">
													<td class="ragwan">รางวัลที่ 1<br>รางวัลละ 6,000,000 บาท</td>
													<td class="ragwan">เลขท้าย 2 ตัว<br>1 รางวัลๆละ 2,000 บาท</td>
													<td class="ragwan">เลขท้าย 3 ตัว<br>2 รางวัลๆละ 4,000 บาท</td>
													<td class="ragwan">เลขหน้า 3 ตัว<br>2 รางวัลๆละ 4,000 บาท</td>
												</tr>
												<tr class="pc_none">
													<td class="ragwan">รางวัลที่ 1 </td>
													<td class="ragwan">เลขท้าย 2 ตัว</td>
													<td class="ragwan">เลขท้าย 3 ตัว</td>
													<td class="ragwan">เลขหน้า 3 ตัว</td>
												</tr>
											</tbody>
										</table>
									</div>
									<br>
									<form action="<?php echo $link_check;?>" method="post">
										<button type="submit" class="btn w-100" style="background-color: #3f6814;     color: #fff;">ตรวจสลากกินแบ่งรัฐบาลงวดนี้เพิ่มเติม</button>
									</form>
									<br><br>
                                    <div class="post-meta">
                                        <h4><U>ตรวจสลากกินแบ่งรัฐบาลย้อนหลัง</U></h4>
                                        <?php
										 $i = 0;
										 $url = "https://www.lottery.co.th/lotto/feed";
										 $rss = simplexml_load_file($url); // XML parser
										 foreach($rss->channel->item as $item) {
											$title = str_replace('ตรวจหวย' , 'ตรวจสลากกินแบ่งรัฐบาล' , $item->title);
											$link_check = str_replace('www.lottery.co.th/lotto' , 'lekden.com/lotto/check' , $item->link);
											$link_check = str_replace('-' , '' , $link_check);  
											$link_check = str_replace('62' , '2562' , $link_check);
											$link_check = str_replace('63' , '2563' , $link_check);
											$link_check = str_replace('64' , '2564' , $link_check);
											$link_check = str_replace('65' , '2565' , $link_check);
											$link_check = str_replace('66' , '2566' , $link_check);										
											if ($i >= 1 and $i < 10) { // parse only 10 items
												print '<h3 style="margin-bottom: 0;"><a href="'.$link_check.'" style="color: #626262; font-size: 17px;" title="'.$title.'">'.$title.'</a></h3><div class="back">'.$item->description.'</div>';?>
												<div class="table-responsive" style="margin-bottom: 5px;">
													 <table>
														 <tbody>
															<tr>
																<td class="ragwan">รางวัลที่ 1</td>
																<td class="ragwan">เลขท้าย 2 ตัว</td>
																<td class="ragwan">เลขท้าย 3 ตัว</td>
																<td class="ragwan">เลขหน้า 3 ตัว</td>
															</tr>
														</tbody>
													</table>
												</div>
											<?php }
										 $i++;
										 }
										 ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

				<!-- ##### form_check ##### -->
				<?php $this->load->view('form_check'); ?>
				<!-- ##### form_check End ##### -->
            </div>
        </div>
    </div>
    <!-- ##### Featured Post Area End ##### -->

    <!-- ##### Popular News Area Start ##### -->
    <div class="popular-news-area section-padding-80-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="section-heading">
                        <h6>Popular News</h6>
                    </div>

                    <div class="row">

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/12.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/13.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/14.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/15.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="section-heading">
                        <h6>Info</h6>
                    </div>
                    <!-- Popular News Widget -->
                    <div class="popular-news-widget mb-30">
                        <h3>4 Most Popular News</h3>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>1.</span> Amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales.</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>2.</span> Consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer.</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>3.</span> Adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo.</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>4.</span> Eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>
                    </div>

                    <!-- Newsletter Widget -->
                    <div class="newsletter-widget">
                        <h4>Newsletter</h4>
                        <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                        <form action="#" method="post">
                            <input type="text" name="text" placeholder="Name">
                            <input type="email" name="email" placeholder="Email">
                            <button type="submit" class="btn w-100">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Popular News Area End ##### -->

    <!-- ##### Video Post Area Start ##### -->
    <div class="video-post-area bg-img bg-overlay" style="background-image: url(img/bg-img/bg1.jpg);">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Single Video Post -->
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-video-post">
                        <img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/video1.jpg" alt="">
                        <!-- Video Button -->
                        <div class="videobtn">
                            <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <!-- Single Video Post -->
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-video-post">
                        <img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/video2.jpg" alt="">
                        <!-- Video Button -->
                        <div class="videobtn">
                            <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <!-- Single Video Post -->
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-video-post">
                        <img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/video3.jpg" alt="">
                        <!-- Video Button -->
                        <div class="videobtn">
                            <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Video Post Area End ##### -->

    <!-- ##### Editorial Post Area Start ##### -->
    <div class="editors-pick-post-area section-padding-80-50">
        <div class="container">
            <div class="row">
                <!-- Editors Pick -->
                <div class="col-12 col-md-7 col-lg-9">
                    <div class="section-heading">
                        <h6>Editor’s Pick</h6>
                    </div>

                    <div class="row">

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/1.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/2.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/3.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/4.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/5.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/6.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- World News -->
                <div class="col-12 col-md-5 col-lg-3">
                    <div class="section-heading">
                        <h6>World News</h6>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/7.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/8.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/9.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/10.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/11.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- ##### Editorial Post Area End ##### -->

    <!-- ##### Footer Add Area Start ##### -->
    <div class="footer-add-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer-add">
                        <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/footer-add.gif" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Footer Add Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <?php $this->load->view('html_bottom'); ?>
	
</body>

</html>