﻿ <style> 
.newsletter-widget select {
    width: 100%;
    height: 50px;
    background-color: #fff;
    font-size: 14px;
    font-style: italic;
    color: #000;
    margin-bottom: 25px;
    border: none;
    padding: 0 25px;
}
.newsletter-widget input {
    width: 100%;
    height: 50px;
    background-color: #fff;
    font-size: 14px;
    font-style: italic;
    color: #000;
    margin-bottom: 25px;
    border: none;
    padding: 0 25px;
}
.newsletter-widget button {
    width: 100%;
    height: 50px;
    background-color: #3f6814;
    font-size: 14px;
    font-style: italic;
    color: #fff;
    border: none;
    padding: 0 25px;
    border-radius: 0;
}
.newsletter-widget h4 {
    color: #fff;
}
</style>              
				<div class="col-12 col-md-6 col-lg-4">

                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex" style="border-bottom: 0px solid #d0d5d8;">
                        <div class="newsletter-widget">
							<h4>ตรวจผลสลากกินแบ่งรัฐบาล</h4>
							<p style="color: #fff;">งวดวันที่</p>
							
							<select type="text" name="id" id="id">							
								<?php
									 foreach($select_date as $item) {
										echo '<option value="'.$item->id.'">'.$item->date_text.'</option>';
									 $i++;
									 }
								?>
							</select>
							<input type="text" name="number_lottory" id="number_lottory" maxlength="6" placeholder="กรอกเลขสลาก" >
							<button type="button" class="btn w-100"  onclick="check_lotto()">ตรวจสลากฯ ของคุณ</button>

							<script>
							function check_lotto() { 
								var id = document.getElementById("id").value;
								var number_lottory = document.getElementById("number_lottory").value;
								if(number_lottory == ""){
									alert('กรุณากรอกเลขสลาก');								
								}
								else{
									if(number_lottory.length < 6){

										alert('กรุณากรอกเลขสลากให้ครบ 6 หลัก');
									}else{
										if(jQuery.isNumeric(number_lottory) == false){
											alert('Please enter numeric value');
										}else{										
											$.ajax({
												type: "POST",
												url: "https://lekden.com/lotto/check_lottory",                                                   
												data:{ id:id , number_lottory:number_lottory },
												success: function(aaa) {
													alert(aaa); 
												}
											});
										}
									}
								}
							}
							</script>
						</div>
                    </div>
                    <div style="margin-bottom: 20px; border-bottom: 2px solid #ed501e;">
                        <h2>เรื่องเด่นในรอบสัปดาห์</h2>
                    </div>
                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/21.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">Health</a>
                            <div class="post-meta">
                                <a href="#" class="post-title">
                                    <h6>Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.</h6>
                                </a>
                                <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/22.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">Finance</a>
                            <div class="post-meta">
                                <a href="#" class="post-title">
                                    <h6>Augue semper congue sit amet ac sapien. Fusce consequat.</h6>
                                </a>
                                <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/23.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">Travel</a>
                            <div class="post-meta">
                                <a href="#" class="post-title">
                                    <h6>Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.</h6>
                                </a>
                                <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/24.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">Politics</a>
                            <div class="post-meta">
                                <a href="#" class="post-title">
                                    <h6>Augue semper congue sit amet ac sapien. Fusce consequat.</h6>
                                </a>
                                <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/24.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">Politics</a>
                            <div class="post-meta">
                                <a href="#" class="post-title">
                                    <h6>Augue semper congue sit amet ac sapien. Fusce consequat.</h6>
                                </a>
                                <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                            </div>
                        </div>
                    </div>
                </div>