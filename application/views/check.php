<!DOCTYPE html>
<html lang="en">

<!-- ##### HTML TOP Area Start ##### -->
<?php $this->load->view('html_top'); ?>
<style>
.single-blog-post.featured-post .post-data {
    padding-top: 0;
}
.single-blog-post .post-data .post-catagory {
    color: #626262;
}
table {
  width: 100%;
  text-align:center;
}
table th {
	display:none;
}
.main tbody td{
  font-size: 30px;
  color: #3f6814;
  width: 25%;
  font-weight: 700;
}
.secondary tbody td{
  font-size: 20px;
  color: #3f6814;
  width: 20%;
  font-weight: 700;
}
.back tbody td {
  font-size: 25px;
  color: #000;
  width: 25%;
  font-weight: 700;
}
tbody .ragwan {
  font-size: 12px;
  color: #000;
  width: 25%;
  font-weight: 500;
  background-color: #f0f0f0;
}
strong{
	display:none;
}
caption{
	display:none;
}
.table-responsive p {
    color: #626262;
	font-size: 18px;
	font-weight: 600;
}
</style>
<body>
    <!-- ##### Header Area Start ##### -->
	<?php $this->load->view('header'); ?>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Featured Post Area Start ##### -->
    <div class="featured-post-area" style="margin-top: 20px;">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-8">
                    <div class="row">

                        <!-- Single Featured Post -->
                        <div class="col-12 col-lg-12">
                            <div class="single-blog-post featured-post">
                                <div class="post-data">			 
									<h2 style="text-align: center;">ผลสลากกินแบ่งรัฐบาล</h2> 
									<h2 style="text-align: center;"><a href="#" class="post-catagory" style="font-size: 20px;"title=""><?php echo $lotto_date;?></a></h2>                              
									
									<div class="table-responsive main">
										<table>
											<tbody>
												<tr>																				
													<?php
													 foreach($check as $el) {
													    $front = str_replace(',' , '&nbsp;&nbsp;' , $el->front); 
													    $back = str_replace(',' , '&nbsp;&nbsp;' , $el->back);
														echo '<td>'.$el->rangwan1.'</td>';
														echo '<td>'.$front.'</td>';
														echo '<td>'.$back.'</td>';
														echo '<td>'.$el->two_digits.'</td>';
													 }
													 ?>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="table-responsive">
										 <table>
											 <tbody>
												<tr>
													<td class="ragwan">รางวัลที่ 1<br>รางวัลละ 6,000,000 บาท</td>
													<td class="ragwan">เลขหน้า 3 ตัว<br>2 รางวัลๆละ 4,000 บาท</td>
													<td class="ragwan">เลขท้าย 3 ตัว<br>2 รางวัลๆละ 4,000 บาท</td>												
													<td class="ragwan">เลขท้าย 2 ตัว<br>1 รางวัลๆละ 2,000 บาท</td>
												</tr>
											</tbody>
										</table>
									</div>
									<br>
                                    
									<div class="table-responsive secondary">
										<p style="text-align: center;">รางวัลข้างเคียงรางวัลที่ 1 มี 2 รางวัลๆละ 100,000 บาท</p>
										<table>
											<tbody>
												<tr>
													<?php
													foreach($check as $el) {
													    $like_rangwan1 = explode(",", $el->like_rangwan1);
														echo '<td>'.$like_rangwan1[0].'</td>';
														echo '<td>'.$like_rangwan1[1].'</td>';
													 }
													?>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="table-responsive secondary">
										<p style="text-align: center;">ผลสลากกินแบ่งรัฐบาล รางวัลที่ 2 มี 5 รางวัลๆละ 200,000 บาท</p>
										<table>
											<tbody>
												<tr>											
													<?php
													foreach($check as $el) {
													    $rangwan2 = explode(",", $el->rangwan2);
													    $arrlength = count($rangwan2);
                                                        for($x = 0; $x < $arrlength; $x++) {
                                                            echo '<td>'.$rangwan2[$x].'</td>';
                                                        }
													 }
													?>									
												</tr>
											</tbody>
										</table>
									</div>	
									<div class="table-responsive secondary">
										<p style="text-align: center;">ผลสลากกินแบ่งรัฐบาล รางวัลที่ 3 มี 10 รางวัลๆละ 80,000 บาท</p>
										<table>
											<tbody>
												<tr>										
													<?php
													foreach($check as $el) {
													    $rangwan3 = explode(",", $el->rangwan3);
													    $arrlength = count($rangwan3);
                                                        for($x = 0; $x < $arrlength; $x++) {
                                                            if($x == 4){
                                                                echo '<td>'.$rangwan3[$x].'</td></tr><tr>';
                                                            }else{
                                                                echo '<td>'.$rangwan3[$x].'</td>';
                                                            }
                                                        }
													 }
													?>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="table-responsive secondary">
										<p style="text-align: center;">ผลสลากกินแบ่งรัฐบาล รางวัลที่ 4 มี 50 รางวัลๆละ 40,000 บ าท</p>
										<table>
											<tbody>
												<tr>
													<?php
													foreach($check as $el) {
													    $rangwan4 = explode(",", $el->rangwan4);
													    $arrlength = count($rangwan4);
                                                        for($x = 0; $x < $arrlength; $x++) {
                                                            if($x == 4){
                                                                echo '<td>'.$rangwan4[$x].'</td></tr><tr>';
                                                            }if($x == 9){
                                                                echo '<td>'.$rangwan4[$x].'</td></tr><tr>';
                                                            }if($x == 14){
                                                                echo '<td>'.$rangwan4[$x].'</td></tr><tr>';
                                                            }if($x == 19){
                                                                echo '<td>'.$rangwan4[$x].'</td></tr><tr>';
                                                            }if($x == 24){
                                                                echo '<td>'.$rangwan4[$x].'</td></tr><tr>';
                                                            }if($x == 29){
                                                                echo '<td>'.$rangwan4[$x].'</td></tr><tr>';
                                                            }if($x == 34){
                                                                echo '<td>'.$rangwan4[$x].'</td></tr><tr>';
                                                            }if($x == 39){
                                                                echo '<td>'.$rangwan4[$x].'</td></tr><tr>';
                                                            }if($x == 44){
                                                                echo '<td>'.$rangwan4[$x].'</td></tr><tr>';
                                                            }if($x != 4 and $x != 9 and $x != 14 and $x != 19 and $x != 24 and $x != 29 and $x != 34 and $x != 39 and $x != 44){
                                                                echo '<td>'.$rangwan4[$x].'</td>';
                                                            }
                                                        }
													 }
													?>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="table-responsive secondary">
										<p style="text-align: center;">ผลสลากกินแบ่งรัฐบาล รางวัลที่ 5 มี 100 รางวัลๆละ 20,000 บาท</p>
										<table>
											<tbody>
												<tr>
													<?php
													foreach($check as $el) {
													    $rangwan5 = explode(",", $el->rangwan5);
													    $arrlength = count($rangwan5);
                                                        for($x = 0; $x < $arrlength; $x++) {
                                                            if($x == 4){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 9){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 14){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 19){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 24){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 29){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 34){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 39){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 44){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 49){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 54){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 59){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 64){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 69){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 74){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 79){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 84){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 89){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x == 94){
                                                                echo '<td>'.$rangwan5[$x].'</td></tr><tr>';
                                                            }if($x != 4 and $x != 9 and $x != 14 and $x != 19 and $x != 24 and $x != 29 and $x != 34 and $x != 39 and $x != 44 and $x != 49 and $x != 54 and $x != 59 and $x != 64 and $x != 69 and $x != 74 and $x != 79 and $x != 84 and $x != 89 and $x != 94){
                                                                echo '<td>'.$rangwan5[$x].'</td>';
                                                            }
                                                        }
													 }
													?>
												</tr>
											</tbody>
										</table>
									</div>
										
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

				<!-- ##### form_check ##### -->
				<?php $this->load->view('form_check'); ?>
				<!-- ##### form_check End ##### -->
            </div>
        </div>
    </div>
    <!-- ##### Featured Post Area End ##### -->

    <!-- ##### Popular News Area Start ##### -->
    <div class="popular-news-area section-padding-80-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="section-heading">
                        <h6>Popular News</h6>
                    </div>

                    <div class="row">

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/12.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/13.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/14.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/15.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="<?php echo base_url(); ?>resource/newspaper/img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="section-heading">
                        <h6>Info</h6>
                    </div>
                    <!-- Popular News Widget -->
                    <div class="popular-news-widget mb-30">
                        <h3>4 Most Popular News</h3>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>1.</span> Amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales.</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>2.</span> Consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer.</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>3.</span> Adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo.</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>4.</span> Eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>
                    </div>

                    <!-- Newsletter Widget -->
                    <div class="newsletter-widget">
                        <h4>Newsletter</h4>
                        <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                        <form action="#" method="post">
                            <input type="text" name="text" placeholder="Name">
                            <input type="email" name="email" placeholder="Email">
                            <button type="submit" class="btn w-100">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Popular News Area End ##### -->

    <!-- ##### Video Post Area Start ##### -->
    <div class="video-post-area bg-img bg-overlay" style="background-image: url(img/bg-img/bg1.jpg);">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Single Video Post -->
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-video-post">
                        <img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/video1.jpg" alt="">
                        <!-- Video Button -->
                        <div class="videobtn">
                            <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <!-- Single Video Post -->
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-video-post">
                        <img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/video2.jpg" alt="">
                        <!-- Video Button -->
                        <div class="videobtn">
                            <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <!-- Single Video Post -->
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-video-post">
                        <img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/video3.jpg" alt="">
                        <!-- Video Button -->
                        <div class="videobtn">
                            <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Video Post Area End ##### -->

    <!-- ##### Editorial Post Area Start ##### -->
    <div class="editors-pick-post-area section-padding-80-50">
        <div class="container">
            <div class="row">
                <!-- Editors Pick -->
                <div class="col-12 col-md-7 col-lg-9">
                    <div class="section-heading">
                        <h6>Editor’s Pick</h6>
                    </div>

                    <div class="row">

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/1.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/2.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/3.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/4.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/5.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/6.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-title">
                                        <h6>Orci varius natoque penatibus et magnis dis parturient montes.</h6>
                                    </a>
                                    <div class="post-meta">
                                        <div class="post-date"><a href="#">February 11, 2018</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- World News -->
                <div class="col-12 col-md-5 col-lg-3">
                    <div class="section-heading">
                        <h6>World News</h6>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/7.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/8.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/9.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/10.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/11.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- ##### Editorial Post Area End ##### -->

    <!-- ##### Footer Add Area Start ##### -->
    <div class="footer-add-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer-add">
                        <a href="#"><img src="<?php echo base_url(); ?>resource/newspaper/img/bg-img/footer-add.gif" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Footer Add Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <?php $this->load->view('html_bottom'); ?>
</body>

</html>